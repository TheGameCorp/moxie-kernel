#include <stddef.h>
#include <kernel/tty.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

void putc (char c) {
    asm ("swi 1");
}

extern volatile unsigned int __kernel_end__;

void main () {
    printf ("Hex test: 0x%x 0x%X\n", 0xCAFE, 0xBEEF);

    printf ("Kernel end: %08X\n", &__kernel_end__);

    while (1);
}
