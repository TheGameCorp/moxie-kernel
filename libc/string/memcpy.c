#include <string.h>

void *memcpy (void *dest, const void *src, size_t len) {
    char *s = (char *) src;
    char *d = (char *) dest;

    while (len-- > 0)
        *d++ = *s++;

    return dest;
}
