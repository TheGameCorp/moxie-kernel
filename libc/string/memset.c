#include <string.h>

void *memset (void *dest, int c, size_t n) {
#if defined (__i386__)
	int tmp = 0;
	asm volatile ("rep stosb"
				: "=c" (tmp)
				: "D" (dest), "a" (c), "c" (n)
                : "flags", "memory");
#elif defined (__moxie__)
    unsigned char *buf = (unsigned char *) dest;
    for (size_t i = 0; i < n; i++)
        buf[i] = (unsigned char) c;
#endif
    return dest;
}
