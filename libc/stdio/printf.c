#include <stddef.h>
#include <stdarg.h>
#if __STDC__HOSTED__
#include <stding-gcc.h>
#else
#include <stdint.h>
#endif
#include <stdio.h>
#include <string.h>

static void print (const char *data, size_t len) {
    for (size_t i = 0; i < len; i++)
        putchar ((int) ((const unsigned char *) data)[i]);
}

int printf (const char *__restrict format, ...) {
    va_list argp;
    va_start (argp, format);

    char tmp_buf[512];
    memset (tmp_buf, 0, 512);

    vsprintf (tmp_buf, format, argp);
    va_end (argp);

    size_t temp = strlen (tmp_buf);
    print (tmp_buf, temp);

    return temp;
}
