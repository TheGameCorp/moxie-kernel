export ARCH:=moxie
export HOST:=moxiebox

export:=$(MAKE:-make)

export AR:=$(HOST)-ar
export AS:=$(HOST)-as
export CC:=$(HOST)-gcc

export PREFIX:=/usr
export EXEC_PREFIX:=$(PREFIX)
export BOOTDIR:=/boot
export LIBDIR:=$(EXEC_PREFIX)/lib
export INCLUDEDIR:=$(PREFIX)/include


export CC:=$(CC) --sysroot=$(PWD)/sysroot -isystem=$(INCLUDEDIR)
export CXX:=$(CXX) --sysroot=$(PWD)/sysroot -isystem=$(INCLUDEDIR) -D__HAS_NO_CRT_INIT
export DESTDIR:=$(PWD)/sysroot

export CFLAGS:=-O0 -g

SYSTEM_HEADER_PROJECTS:=libc kernel
PROJECTS:=$(SYSTEM_HEADER_PROJECTS)

.PHONY: all kernel clean headers

kernel: headers
	for PROJECT in $(PROJECTS); do \
		make -s -C $$PROJECT install ; \
	done

headers:
	mkdir -p sysroot
	for PROJECT in $(SYSTEM_HEADER_PROJECTS); do \
		make -s -C $$PROJECT install-headers ; \
	done

clean:
	for PROJECT in $(PROJECTS); do \
		make -s -C $$PROJECT clean ; \
	done
	rm -rf sysroot