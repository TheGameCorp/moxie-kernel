#ifndef _STRING_H
#define _STRING_H 1

#include <sys/cdefs.h>

#include <stddef.h>
#include <limits.h>

#define ALIGN (sizeof (size_t))
#define ONES ((size_t) -1 / UCHAR_MAX)
#define HIGHS (ONES * (UCHAR_MAX / 2 + 1))
#define HASZERO(x) (((X) - ONES) & -(X) & HIGHS)

void *memset (void *, int, size_t);
void *memcpy (void *, const void *, size_t);

size_t strlen (const char *);

#endif
